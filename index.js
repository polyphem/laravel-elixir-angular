var gulp = require('gulp');
var Elixir = require('laravel-elixir');
var ts = require('gulp-typescript');

var $ = Elixir.Plugins;
var config = Elixir.config;

var angularOptions = {
    "target": "es5",
    "module": "system",
    "moduleResolution": "node",
    "sourceMap": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "removeComments": false,
    "noImplicitAny": false
};

Elixir.extend('angular', function(output, baseDir, options) {
    var paths = prepGulpPaths('/**/*.ts', baseDir, output);

    new Elixir.Task('angular', function() {
        var typescriptOptions = options || angularOptions; //config.js.angular.options

        return gulpTask.call(this, paths, typescriptOptions);
    })
    .watch(paths.src.baseDir + '/**/*.ts')
    .ignore(paths.output.path);
});

/**
 * Trigger the Gulp task logic.
 *
 * @param {GulpPaths}   paths
 * @param {object|null} angular
 */
var gulpTask = function(paths, angular) {
    this.log(paths.src, paths.output);

    return (
        gulp
            .src(paths.src.path)
            .pipe($.if(config.sourcemaps, $.sourcemaps.init()))
            .pipe(ts(angular))
            .on('error', function(e) {
                new Elixir.Notification().error(e, 'Angular Compilation Failed!');
                this.emit('end');
            })
            .pipe($.if(config.production, $.uglify({compress: { drop_console: true }})))
            .pipe($.if(config.sourcemaps, $.sourcemaps.write('.')))
            .pipe(gulp.dest(paths.output.baseDir))
            .pipe(new Elixir.Notification('Angular Compiled!'))
    );
};

/**
 * Prep the Gulp src and output paths.
 *
 * @param  {string|Array} src
 * @param  {string|null}  baseDir
 * @param  {string|null}  output
 * @return {GulpPaths}
 */
var prepGulpPaths = function(src, baseDir, output) {
    return new Elixir.GulpPaths()
        .src(src, baseDir || config.get('assets.js.folder'))
        .output(output || config.get('public.js.outputFolder'), 'main.js');
};